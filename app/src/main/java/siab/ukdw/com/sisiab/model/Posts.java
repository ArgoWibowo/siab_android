package siab.ukdw.com.sisiab.model;

import com.google.gson.annotations.SerializedName;

public class Posts {
    @SerializedName("userId")
    private String userId;

    @SerializedName("id")
    private Integer id;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    public Posts(String userId, Integer id, String title, String body) {
        this.setUserId(userId);
        this.setId(id);
        this.setTitle(title);
        this.setBody(body);
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
