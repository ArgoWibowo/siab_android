package siab.ukdw.com.sisiab.network;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import siab.ukdw.com.sisiab.model.Posts;

public interface GetDataService{
    @GET("/posts")
    Call<List<Posts>> getAllPosts();
}
