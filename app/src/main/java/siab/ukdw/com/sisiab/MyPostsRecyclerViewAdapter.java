package siab.ukdw.com.sisiab;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import siab.ukdw.com.sisiab.model.Posts;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * TODO: Replace the implementation with code for your data type.
 */
public class MyPostsRecyclerViewAdapter extends RecyclerView.Adapter<MyPostsRecyclerViewAdapter.ViewHolder> {

    private List<Posts> mPosts;

    public MyPostsRecyclerViewAdapter(List<Posts> mPosts) {
        this.mPosts = mPosts;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_posts, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txtNama.setText(mPosts.get(position).getBody());
        holder.txtNpm.setText(mPosts.get(position).getTitle());
        holder.txtNoHp.setText(mPosts.get(position).getUserId());
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNama, txtNpm, txtNoHp;

        public ViewHolder(View view) {
            super(view);
            txtNama = (TextView) view.findViewById(R.id.txt_nama_mahasiswa);
            txtNpm = (TextView) view.findViewById(R.id.txt_npm_mahasiswa);
            txtNoHp = (TextView) view.findViewById(R.id.txt_nohp_mahasiswa);
        }
    }

    public void clear(){
        mPosts.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<Posts> newPost){
        mPosts.addAll(newPost);
        notifyDataSetChanged();
    }
}
